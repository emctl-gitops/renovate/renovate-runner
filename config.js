module.exports = {
        endpoint: 'https://gitlab.com/api/v4/',
        platform: 'gitlab',
        persistRepoData: true,
        logLevel: 'debug',
        onboardingConfig: {
                'extends': [
                        "gitlab>emctl-gitops/renovate/renovate-config:config"
                ],
        },
        autodiscover: false

};
